let username;
let password;
let role;

function login() {

    username = prompt("Enter your username:");
    password = prompt("Enter your password:");
    role = prompt("What is your role?");

    if(username === "" || password === "" || role === "") {

        alert("Input should not be empty");

    }

    switch(role) {

        case "admin":
            alert("Welcome back to the class portal, admin!");
            break;
        case "teacher":
            alert("Thank you for logging in, teacher!");
            break;
        case "student":
            alert("Welcome to the class portal, student!");
            break;
        default:
            alert("Role out of range.");
            break;

    }


}

function checkAverage(grade1,grade2,grade3,grade4) {

    let average = Math.round((grade1 + grade2 + grade3 + grade4) / 4);

    console.log(average);

    if (average <= 74) {

        console.log("Hello, student, your average is " + average + ". The letter equivalent is F");

    } else if (average >= 75 && average <= 79) {

        console.log("Hello, student, your average is " + average + ". The letter equivalent is D");

    } else if (average >= 80 && average <= 84) {

        console.log("Hello, student, your average is " + average + ". The letter equivalent is C");

    } else if (average >= 85 && average <= 89) {

        console.log("Hello, student, your average is " + average + ". The letter equivalent is B");

    } else if (average >= 90 && average <= 95) {

        console.log("Hello, student, your average is " + average + ". The letter equivalent is A");

    } else if (average >= 96 && average <= 100) {

        console.log("Hello, student, your average is " + average + ". The letter equivalent is A+");

    } else {

        console.log("Invalid Grade");

    }

}